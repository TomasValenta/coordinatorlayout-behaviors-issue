package cz.yetanotherview.coordinatorlayoutbehaviors.behaviours;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import cz.yetanotherview.coordinatorlayoutbehaviors.views.TryMeButton;

@SuppressWarnings("unused")
public class TranslationBehavior extends CoordinatorLayout.Behavior<View> {

    private static final int TRANSLATION_MULTIPLIER = 44;

    public TranslationBehavior(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(final CoordinatorLayout parent, final View child, final View dependency) {
        return dependency instanceof TryMeButton;
    }

    @Override
    public boolean onDependentViewChanged(final CoordinatorLayout parent, final View child, final View dependency) {
        final float translationY = -dependency.getTranslationY() * TRANSLATION_MULTIPLIER;
        child.setTranslationY(translationY);
        ((TextView) ((LinearLayout) child).getChildAt(1)).setText(String.format(Locale.UK, "%.3f", translationY));
        return true;
    }
}
